module FeatureHelper
  def login_admin # це є метод, який ми можемо викликати у step definitions
    @login_page = LoginPage.new
    @login_page.load
    @login_page.username_field.set 'user'
    @login_page.password_field.set 'ZnrooZB6dFhE'
    @login_page.submit_button.click
  end

  def login(user_name, password)
    @login_page = LoginPage.new
    @login_page.load
    @login_page.username_field.set user_name
    @login_page.password_field.set password
    sleep 4
    @login_page.submit_button.click
  end

  def user_registration(login_name, password, first_name, last_name, email)
    @registeration_page = RegistrationPage.new
    @registeration_page.load
    @registeration_page.login_name_field.set login_name
    @registeration_page.password_field.set password
    @registeration_page.confirm_password_field.set password
    @registeration_page.first_name_field.set first_name
    @registeration_page.last_name_field.set last_name
    @registeration_page.email_field.set email
    @registeration_page.language_field.set value='en-GB'
  end

  def log_out
    @home_page = MyHomePage.new
    @home_page.top_menu.sign_out_link.click
  end
end