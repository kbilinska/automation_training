Feature: Sign out
  As a user
  I want to be able to sign out
  In order to become a logged out user

  Scenario:  Sign out positive flow
     Given I am not logged in user
      When I click sign in button
      When I login with valid credentials
       And I click log in button
      When I click sign out button
      Then I became a logged out user