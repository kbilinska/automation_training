Feature: Registration
  As a user
  I want to be able to register
  In order to become a user
  Scenario: Registration
     Given I do not have account
      When I click on submit button
      Then I become a registered and logged in as user
      When I click sign out button
      Then I became a logged out user
      When I am logged in as registered user
      Then I become a logged in as registered user