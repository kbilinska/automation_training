Feature: Sign in
  User story
  As a user
  I want to be able to sign in
  In order to become a user

#  background steps виконуються кожного разу перед кожним сценарієм для цієї feature
#  Background:

  Scenario:  Sign in positive flow
    Given I am not logged in user

     When I click sign in button

     Then I see the log in form

     When I login with valid credentials
      And I click log in button
     Then I become a logged in as "user"