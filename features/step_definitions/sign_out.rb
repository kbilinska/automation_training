When(/^I click sign out button$/) do
  log_out
end

Then(/^I became a logged out user$/) do
  expect(page).to have_content 'Sign in'
  sleep 2
end