Given(/^I do not have account$/) do
  @login_name = 'user_' + Time.now.to_i.to_s
  @email = @login_name + '@test.com'
  @password = 'qwerty1234'
  user_registration(@login_name, @password, 'Tom', 'Tester', @email)
  sleep 2
end

When(/^I click on submit button$/) do
  @registeration_page.submit_button.click
  sleep 2
end

Then(/^I become a registered and logged in as user$/) do
  expect(page).to have_content "Your account has been activated. You can now log in."
  sleep 2
end

When(/^I am logged in as registered user$/) do
  login(@login_name, @password)
end

Then(/^I become a logged in as registered user$/) do
  expect(page).to have_content "Logged in as #{@login_name}" #інтерполяція - завжди у ""
  sleep 2
end
