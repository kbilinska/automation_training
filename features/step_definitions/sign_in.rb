Given(/^I am not logged in user$/) do
  #visit 'http://10.131.8.24/'
  @home_page = MyHomePage.new
  @home_page.load
  sleep 1
end

When(/^I click sign in button$/) do
  #find(:xpath, '//*[@id="account"]/ul/li[1]/a' ).click
  @home_page.top_menu.sign_in_link.click
  #
  sleep 1
end

Then(/^I see the log in form$/) do
  expect(current_url).to include '/login'
  expect(page).to have_content 'Login'
  expect(page).to have_content 'Password'
end

When(/^I login with valid credentials$/) do
  #find('#username').set 'user'
  #find('#password').set 'ZnrooZB6dFhE'
  @login_page = LoginPage.new
  @login_page.username_field.set 'user'
  @login_page.password_field.set 'ZnrooZB6dFhE'

  sleep 2
end

And(/^I click log in button$/) do
  #find('#login-submit').click
  @login_page.submit_button.click
end

Given(/^I am logged in as "([^"]*)"$/) do |user_name|
#  прийшло з sign_in_helper.feature
  login(user_name, 'qwerty1234')
  sleep 4
end

Then(/^I become a logged in as "([^"]*)"$$/) do |user_name|
  expect(page).to have_content "Logged in as #{user_name}" #інтерполяція - завжди у ""
  sleep 2
end

#Given(/^I am logged in user$/) do
#  login_admin
#end

#Given(/^I am logged in user$/) do
#  login('testuser1234', 'qwerty1234')
#end

