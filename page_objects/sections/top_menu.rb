class TopMenu < SitePrism::Section
  element :sign_in_link, :xpath, '//*[@id="account"]//a[@class="login"]'
  element :sign_out_link, :xpath, '//*[@id="account"]//a[@class="logout"]'
end
