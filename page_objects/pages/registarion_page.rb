class RegistrationPage < SitePrism::Page
  set_url 'http://10.131.8.24/account/register'

  element :login_name_field, '#user_login'
  element :password_field, '#user_password'
  element :confirm_password_field, '#user_password_confirmation'
  element :first_name_field, '#user_firstname'
  element :last_name_field, '#user_lastname'
  element :email_field, '#user_mail'
  element :language_field, '#user_language'
  element :submit_button, :xpath, '//*[@id="new_user"]/input[@type="submit"]'

  section :top_menu, TopMenu, '#top-menu'
end