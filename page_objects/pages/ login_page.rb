class LoginPage < SitePrism::Page
  set_url 'http://10.131.8.24/login'

  element :username_field,'#username'
  element :password_field, '#password'
  element :submit_button, '#login-submit'
end